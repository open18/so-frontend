import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Navbar from './components/Navbar';
import { sendTextAction } from './redux/test/testAction';
import axios from 'axios';
import { URL_API } from './service/config';

const App = () => {

    //internal state 
    const [ping, setPing] = React.useState(false);


    const dispatch = useDispatch();

    //actions
    const sendText = text => dispatch(sendTextAction(text));

    //state
    const texts = useSelector(state => state.test.texts);
    //const loading = useSelector(state => state.test.loading);

    //hacemos un ping al Dyno de heroku para que se inicie.
    React.useEffect(() => {
        axios.get(URL_API + "ping")
            .then(res => setPing(true))
            .catch(err => console.error(err))
    }, []);


    return (
        <div>
            <Navbar
                sendText={text => sendText(text)}
                ping={ping}
            />
            {(!ping) && (<h4 style={{ textAlign: "center" }}>Iniciando el Dyno de Heroku...</h4>)}
            <div className="card custom-card">
                <div className="card-body">
                    <h3 className="title">Results</h3>
                </div>
                <div className="card-body">
                    <div className="container">
                        <div className="row">
                            {[...texts].reverse().map((item, key) => (
                                <div className="col-12 custom-padding" key={key + "_" + item.palindrome + "_" + item.text}>
                                    <input
                                        className="form-control"
                                        type="text"
                                        defaultValue={item.text}
                                        readOnly
                                    />
                                    {item.palindrome && <span style={{ float: "right" }}>Es palindroma</span>}
                                    <hr />
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;