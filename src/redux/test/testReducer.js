import {
    LOADING_SEND_TEXT,
    SUCCESS_SEND_TEXT,
    FAILURE_SEND_TEXT
} from './type'


const initialState = {
    loading: false,
    error: null,
    texts: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOADING_SEND_TEXT:
            return {
                ...state,
                loading: action.payload
            }
        case FAILURE_SEND_TEXT:
            console.error(action.payload);
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case SUCCESS_SEND_TEXT:
            return {
                ...state,
                loading: false,
                error: null,
                texts: action.payload
            }
        default: return state
    }
}

export default reducer;
