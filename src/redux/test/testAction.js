import axios from "axios";
import { URL_API } from "./../../service/config";

import {
    LOADING_SEND_TEXT,
    SUCCESS_SEND_TEXT,
    FAILURE_SEND_TEXT
} from './type';



export const sendTextAction = text => {
    return async (dispatch, getState) => {
        dispatch(handleDispatch(LOADING_SEND_TEXT, true));

        var config = {
            method: 'get',
            url: `${URL_API}iecho?text=${text}`
        };

        const { texts } = getState().test;

        axios(config).then(res => {
            const newTexts = [...texts];
            newTexts.push({
                ...res.data,
                date: new Date()
            });
            dispatch(handleDispatch(SUCCESS_SEND_TEXT, newTexts));
        }).catch(err => {
            dispatch(handleDispatch(FAILURE_SEND_TEXT, err.message));
        });
    }
}

const handleDispatch = (type, payload) => ({ type, payload });
