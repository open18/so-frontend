import React from 'react';

const Navbar = ({ sendText, ping }) => {

    //internal state
    const [text, setText] = React.useState("");

    const send = e => {
        e.preventDefault();
        if (text && text !== "") {
            setText("");
            sendText(text);
        }
    }

    return (
        <nav className="navbar navbar-expand-lg" style={{ backgroundColor: "red", textAlign: "center" }}>
            <form className="form-inline"
                style={{ textAlign: "center", width: "100%" }}
                onSubmit={send}
            >
                <div style={{ width: "100%" }}>
                    <input
                        value={text}
                        onChange={e => setText(e.target.value)}
                        className="form-control mr-sm-2"
                        type="text"
                        placeholder="Insert text"
                        style={{ width: "50%" }}
                    />
                    <button className="btn btn-blue" type="submit" disabled={!ping}>Send</button>
                </div>
            </form>
        </nav>
    );
};

export default Navbar;