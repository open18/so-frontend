## Tech Stack

- ReactJS
- Redux
- Bootstrap
- Axios

## Demo

https://master.d2kj8htz4i4590.amplifyapp.com/

## Available Scripts

In the project directory, you can run:

### `npm start`

Se inicia el servidor de dev

### `npm run test`

Se ejecutan las pruebas unitarias

### `npm build`

Para generar una version de produccion

